const path = require('path');
const HtmlWebpackPlugin = require("html-webpack-plugin");
const WorkboxPlugin = require("workbox-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const WebpackPwaManifest = require("webpack-pwa-manifest");

const outputDir = path.join(__dirname, "build/");
const isProd = process.env.NODE_ENV === 'production';

module.exports = {
  entry: './lib/es6/src/Index.bs.js',
  mode: isProd ? 'production' : 'development',
  output: {
    path: outputDir,
    filename: '[name].[chunkhash].js',
  },
    module: {
    rules: [
      { test: /\.scss$/, use: ["style-loader", "css-loader", "sass-loader"] },
      { test: /\.(png|jpg|svg)$/, use: "url-loader" }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./public/index.html"
    }),
    new WorkboxPlugin.InjectManifest({
      swSrc: "./src/sw.js"
    }),
    new WebpackPwaManifest({
      name: "DCS App",
      short_name: "App",
      description: "Web dashboard application for DCS",
      background_color: "#ffffff",
      icons: [
        {
          src: path.resolve("src/img/dcs.png"),
          size: "400x400"
        }
      ]
    }),
    new CleanWebpackPlugin(["build"], {
      watch: true
    })
  ]
};
