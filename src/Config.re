type routes =
  | Home;

let routeToString =
  fun
  | Home => "/";

let urlToRoute: ReasonReact.Router.url => routes =
  url =>
    switch (url.path) {
    | _ => Home
    };

let routeToTitle = route =>
  switch (route) {
  | Home => "Home"
  };

let routeToComponent = currentRoute => {
  let withCSSTransition = (component, route) =>
    <ReactTransitionGroup.CSSTransition
      show=true
      timeout=900
      key=(routeToTitle(route))
      classNames="routeTransition">
      component
    </ReactTransitionGroup.CSSTransition>;
  switch (currentRoute) {
  | Home => withCSSTransition(<Home />, Home)
  };
};
