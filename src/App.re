[%bs.raw {|require('../../../src/styles/App.scss')|}];

[@bs.val] [@bs.scope "performance"] external now : unit => float = "";

type nav = {
  isOpen: bool,
  isSwiping: ref(bool),
  position: list((float, float)),
  width: ref(float),
};

type state = {
  nav,
};

type action =
  | ToggleMenu(bool);

let component = ReasonReact.reducerComponent("App");

let make = (~currentRoute, _children) => {
  ...component,
  initialState: () => {
    nav: {
      isOpen: false,
      isSwiping: ref(false),
      position: [(0.0, 0.0)],
      width: ref(0.0),
    },
  },
  willReceiveProps: self => {
    ...self.state,
    nav: {
      ...self.state.nav,
      isOpen: false,
    },
  },
  reducer: (action, state) => {
    switch (action) {
    | ToggleMenu(isOpen) =>
      ReasonReact.UpdateWithSideEffects(
        {
          ...state,
          nav: {
            ...state.nav,
            isOpen,
          },
        },
        (
          _self =>
            if (isOpen) {
              %bs.raw
              {| document.documentElement.style.overflow = "hidden"|};
              /* ReactDOMRe.domElementToObj(documentElement)##style##overflow#="hidden"; */
            } else {
              %bs.raw
              {| document.documentElement.style.overflow = ""|};
              /* ReactDOMRe.domElementToObj(documentElement)##style##overflow#=""; */
            }
        ),
      )
    }
  },
  render: self => {
    let (x, _t) = List.hd(List.rev(self.state.nav.position));
    let (x', _t') = List.hd(self.state.nav.position);
    <div
      className=("App" ++ (self.state.nav.isOpen ? " overlay" : ""))
      onClick=(_event => self.send(ToggleMenu(false)))
    >
      <header>
        <a
          onClick=(
            event => {
              ReactEvent.Mouse.stopPropagation(event);
              self.send(ToggleMenu(! self.state.nav.isOpen));
            }
          )>
          <svg viewBox="0 0 24 24">
            <path
              d="M21 11h-18c-0.6 0-1 0.4-1 1s0.4 1 1 1h18c0.6 0 1-0.4 1-1s-0.4-1-1-1z"
            />
            <path
              d="M3 7h18c0.6 0 1-0.4 1-1s-0.4-1-1-1h-18c-0.6 0-1 0.4-1 1s0.4 1 1 1z"
            />
            <path
              d="M21 17h-18c-0.6 0-1 0.4-1 1s0.4 1 1 1h18c0.6 0 1-0.4 1-1s-0.4-1-1-1z"
            />
          </svg>
        </a>
        <h1> (ReasonReact.string(Config.routeToTitle(currentRoute))) </h1>
      </header>
      <nav
        className=(self.state.nav.isOpen ? "active" : "")
        onClick=(event => ReactEvent.Mouse.stopPropagation(event))
        style=(
          self.state.nav.isSwiping^ ?
            ReactDOMRe.Style.make(
              ~transform=
                "translateX("
                ++ string_of_float(x' -. x > 0.0 ? 0.0 : x' -. x)
                ++ "0px)",
              ~transition="none",
              (),
            ) :
            ReactDOMRe.Style.make()
        )
        ref=(
          self.handle((ref, self) =>
            self.state.nav.width :=
              (
                switch (Js.Nullable.toOption(ref)) {
                | None => 0.0
                | Some(r) => ReactDOMRe.domElementToObj(r)##clientWidth
                }
              )
          )
        )>
        <header>
          <a onClick=(_event => self.send(ToggleMenu(false)))>
            <svg viewBox="0 0 32 32">
              <path
                d="M12.586 27.414l-10-10c-0.781-0.781-0.781-2.047 0-2.828l10-10c0.781-0.781 2.047-0.781 2.828 0s0.781 2.047 0 2.828l-6.586 6.586h19.172c1.105 0 2 0.895 2 2s-0.895 2-2 2h-19.172l6.586 6.586c0.39 0.39 0.586 0.902 0.586 1.414s-0.195 1.024-0.586 1.414c-0.781 0.781-2.047 0.781-2.828 0z"
              />
            </svg>
            (ReasonReact.string(Config.routeToTitle(currentRoute)))
          </a>
          <svg viewBox="0 0 32 32">
            <path
              d="M8 10c0-4.418 3.582-8 8-8s8 3.582 8 8c0 4.418-3.582 8-8 8s-8-3.582-8-8zM24 20h-16c-4.418 0-8 3.582-8 8v2h32v-2c0-4.418-3.582-8-8-8z"
            />
          </svg>
        </header>
        <label> (ReasonReact.string("home")) </label>
        /*
         *<ul>
         *  <li>
         *    <Router.NavLink route=Home>
         *      (ReasonReact.string("Home"))
         *    </Router.NavLink>
         *  </li>
         *</ul>
         */
        <label> (ReasonReact.string("pages")) </label>
      </nav>
      <main>
        <ReactTransitionGroup.TransitionGroup>
          (Config.routeToComponent(currentRoute))
        </ReactTransitionGroup.TransitionGroup>
      </main>
    </div>;
  },
};
